# lekture
lekture is an osc sequencer

Development is done under python 2.7.11.
I develop on OSX.X, but lekture should work on Linux and Windows too, but I didn't yet test it .

##User
this is a work in progress… don't expect a working version in the following weeks. But if you want to test it, , just donwload the [latest release](http://github.com/PixelStereo/lekture/releases/latest) and double-click on it to start !!

##Developper
[![Build Status](https://travis-ci.org/PixelStereo/lekture.svg?branch=master)](https://travis-ci.org/PixelStereo/lekture)
[![Coverage Status](https://coveralls.io/repos/PixelStereo/lekture/badge.svg?branch=master&service=github)](https://coveralls.io/github/PixelStereo/lekture?branch=master)
[![Code Climate](https://codeclimate.com/github/PixelStereo/lekture/badges/gpa.svg)](https://codeclimate.com/github/PixelStereo/lekture)
[![Issue Count](https://codeclimate.com/github/PixelStereo/lekture/badges/issue_count.svg)](https://codeclimate.com/github/PixelStereo/lekture)
###Install
You need to install PyQt5, PyOSC and PyBonjour module. The simplest way to install PyQt5 is with using brew.

####Install Brew (OSX)

    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

####Install Dependancies

    brew install python
    brew install PyQt5 --with-python --without-python3

Now you can checkout the repo

    git clone --recursive https://github.com/PixelStereo/lekture.git
    
##Licence
GPL Licence

##Credits
* *Produced and Developped by Pixel Stereo*
* *icon by WOLF LΔMBERT*

